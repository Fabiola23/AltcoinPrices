# CHANGELOG

### Version 1.4.13a
* Bugfix

### Version 1.4.13
* Fix cosmetic issues

### Version 1.4.12
* Add Iconomi DAA (Digital Asset Arrays)

### Version 1.4.11b
* Fix a typo that makes the app crash when Particl is selected from menu

### Version 1.4.11b
* Bugfix

### Version 1.4.11
* Add BlackCoin and Particl

### Version 1.4.10
* Bugfix and code cleanup

### Version 1.4.9
* Add NANO

### Version 1.4.8b
* Bugfix - Augur graph was not loaded correctly

### Version 1.4.8a
* Fix missing translations error

### Version 1.4.8
* Bugfix

### Version 1.4.7
* Add some new coins

### Version 1.4.6
* Replace Reload and Add_New_Item buttons

### Version 1.4.5a
* Bugfix

### Version 1.4.5
* Improve progress bar loading

### Version 1.4.4
* Add donations

### Version 1.4.3
* Bugfix

### Version 1.4.2
* Add some new coins
* Bugfix

### Version 1.4.1
* Add method to get data from Binance
* Add EOS cryptocurrency
* Fix a bug that was preventing certain graphs to be displayed

### Version 1.4
* Improve altcoin data retrieval procedure
* Add and remove some coins (will be added back later on)

### Version 1.3.5
* Redesigne period setting in Settings Activity

### Version 1.3.4
* Fix a bug in unit price calculation when switching global currency and subsequenty adding a cryptocurrency that was already present in portfolio
* Display currency in edit page

### Version 1.3.3
* Fix a bug that cause the app to crash when entering an invalid number

### Version 1.3.2
* Remove unnecessary data reload when going back to main activity

### Version 1.3.1
* Fix crash when selecting DASH from left panel menu

### Version 1.3
* Improve coin's data retrieval

### Version 1.2
* Fix some bugs
* Add Cardano (ADA)

### Version 1.1a
* Fix some bugs and improve user experience

### Version 1.1
* Add Portfolio tracker functionalities

### Version 0.3i
* Add BAT price in USD

### Version 0.3h
* Add XVG price in USDT

### Version 0.3g
* Add XLM price in USD

### Version 0.3f
* Fix Coinbase XBT typo

### Version 0.3e
* Switch from Bitfinex to Coinbase for BTCUSD

### Version 0.3d
* Fix error preventing the chart from loading correctly

### Version 0.3c
* Add Bitfinex price in USD

### Version 0.3b
* Add Bollinger bands indicator
* Add Bitcoin price in USD

###Version 0.3a
* Code refactoring

### Version 0.3
* Change graph source
* Add candlestick period setting

### Version 0.2
* Code refactoring

### Version 0.1
* Initial release
