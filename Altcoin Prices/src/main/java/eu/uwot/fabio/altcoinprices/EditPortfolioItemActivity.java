package eu.uwot.fabio.altcoinprices;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EditPortfolioItemActivity extends AppCompatActivity {

    private String altcoinName;
    private EditText amountBought_text;
    private float amountBought;
    private String altcoinCurrency;
    private EditText unitValue_text;
    private float unitValue;
    private Coin coin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_portfolio_item);

        // extract data from bundle sent from PortfolioActivity
        TextView altcoinName_text = findViewById(R.id.altcoinName);
        Bundle b = getIntent().getExtras();
        if(b != null) {
            altcoinName = b.getString("altcoinDescription");
            amountBought = b.getFloat("amountBought");
            unitValue = b.getFloat("unitValue");
            altcoinCurrency = b.getString("altcoinCurrency");
        }

        // set Altcoin name //
        coin = new Coin(getApplicationContext());
        altcoinName_text.setText(coin.coinsLabelDescriptionHashtable.get(altcoinName));

        // set Amount bought //
        amountBought_text = findViewById(R.id.amountBought_text);
        amountBought_text.setHint(Float.toString(amountBought));

        amountBought_text.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // do nothing
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // do nothing
            }

            @Override
            public void afterTextChanged(Editable s) {
                String getAmountBougt = amountBought_text.getText().toString();
                if (!getAmountBougt.equals("") && !getAmountBougt.equals(".")) {
                    amountBought = Float.parseFloat(getAmountBougt);
                } else {
                    amountBought = -1f;
                }
            }
        });

        // set Unit price //
        TextView altcoinCurrency_text = findViewById(R.id.unitPrice);
        altcoinCurrency_text.append(" (" + altcoinCurrency + ")");

        unitValue_text = findViewById(R.id.unitPrice_text);
        unitValue_text.setHint(Float.toString(unitValue));

        unitValue_text.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // do nothing
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // do nothing
            }

            @Override
            public void afterTextChanged(Editable s) {
                String getUnitValue = unitValue_text.getText().toString();
                if (!getUnitValue.equals("") && !getUnitValue.equals(".")) {
                    unitValue = Float.parseFloat(getUnitValue);
                } else {
                    unitValue = -1f;
                }
            }
        });

        // Save Button //
        final Button saveButton = findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if ((amountBought != -1f) && (unitValue != -1f)) {
                    boolean res = coin.editItem(altcoinName, amountBought, unitValue);
                    if (res == true) {
                        // if value are legal go back to Portfolio activity //
                        //finish();
                        startActivity(new Intent(getApplicationContext(), LoadingActivity.class));
                    }
                }
            }
        });

        // Delete Button //
        final Button deleteButton = findViewById(R.id.deleteButton);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            coin.removeItem(altcoinName);
            //finish();
            startActivity(new Intent(getApplicationContext(), LoadingActivity.class));
            }
        });
    }

}