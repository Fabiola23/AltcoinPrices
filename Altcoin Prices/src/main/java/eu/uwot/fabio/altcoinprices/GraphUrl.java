package eu.uwot.fabio.altcoinprices;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

class GraphUrl {

    private final String URL0 = "<!-- TradingView Widget BEGIN -->\n" +
            "<script type=\"text/javascript\" src=\"https://s3.tradingview.com/tv.js\"></script>\n" +
            "<script type=\"text/javascript\">\n" +
            "new TradingView.widget({\n" +
            "  \"autosize\": true,\n" +
            "  \"symbol\": \"";
    private final String URL1 = "\",\n" +
            "  \"interval\": ";
    private final String URL2 = ",\n" +
            "  \"timezone\": \"Europe/Rome\",\n" +
            "  \"theme\": \"Light\",\n" +
            "  \"style\": \"1\",\n" +
            "  \"locale\": \"en\",\n" +
            "  \"toolbar_bg\": \"#f1f3f6\",\n" +
            "  \"enable_publishing\": false,\n" +
            "  \"withdateranges\": true,\n" +
            "  \"save_image\": false,\n" +
            "  \"hideideas\": true,\n" +
            "  \"studies\": [\n" +
            "    \"BB@tv-basicstudies\",\n" +
            "    \"RSI@tv-basicstudies\"\n" +
            "  ]\n" +
            "});\n" +
            "</script>\n" +
            "<!-- TradingView Widget END -->\n";

    private final Context context;

    public GraphUrl(Context context) {
        this.context = context.getApplicationContext();
    }


    public String getUrl(String altcoinName) {
        SharedPreferences prefs = context.getApplicationContext().getSharedPreferences("Settings", 0); // 0 for private mode
        Coin coin = new Coin(context);
        String period = prefs.getString("period", "30");

        String exchange = coin.coinsLabelExchangeHashtable.get(altcoinName);
        String currency = prefs.getString("currency", "EUR");

        String url;

        switch (altcoinName) {
            case "BTCUSD":
                url = URL0 + exchange + ":" + altcoinName.toUpperCase() + URL1 + period + URL2;
                break;
            case "ETHUSD":
                url = URL0 + exchange + ":" + altcoinName.toUpperCase() + URL1 + period + URL2;
                break;
            case "BTC":
                url = URL0 + exchange + ":" + altcoinName.toUpperCase() + currency + URL1 + period + URL2;
                break;
            case "BCH":
                url = URL0 + exchange + ":" + altcoinName.toUpperCase() + currency + URL1 + period + URL2;
                break;
            case "ETH":
                url = URL0 + exchange + ":" + altcoinName.toUpperCase() + currency + URL1 + period + URL2;
                break;
            case "LTC":
                url = URL0 + exchange + ":" + altcoinName.toUpperCase() + currency + URL1 + period + URL2;
                break;
            default:
                url = URL0 + exchange + ":" + altcoinName.toUpperCase() + coin.coinsLabelGraph.get(altcoinName) + URL1 + period + URL2;
                break;
        }

        /*switch (altcoinName) {
            case "BTCUSD":
                url = URL0 + exchange + ":" + altcoinName.toUpperCase() + URL1 + period + URL2;
                break;
            case "ETHUSD":
                url = URL0 + exchange + ":" + altcoinName.toUpperCase() + URL1 + period + URL2;
                break;
        }*/

        return url;
    }

}
