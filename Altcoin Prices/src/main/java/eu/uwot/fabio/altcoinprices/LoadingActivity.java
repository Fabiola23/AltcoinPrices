package eu.uwot.fabio.altcoinprices;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

public class LoadingActivity extends AppCompatActivity {

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        progressBar = findViewById(R.id.progressBar);

        Thread retrieveData = new Thread() {
            public void run() {
                setPortfolioData();

                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        };

        retrieveData.start();
    }

    private void setPortfolioData () {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("Settings", 0); // 0 for private mode
        SharedPreferences.Editor editor = prefs.edit();
        Coin coin = new Coin(getApplicationContext());

        int portfolioItems = 0;
        for (int i = 0; i < coin.coins.length; i ++) {
            float amountBought = prefs.getFloat(coin.coins[i] + "_a", -1f);

            if (amountBought != -1f) {
                portfolioItems += 1;
            }
        }

        int progressBarSlotTime = 100 / (portfolioItems + 4);
        int progressBarProgess = 0;

        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btcusd = coin.getCoinQuoteCoinbase("BTC", "USD");
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btceur = coin.getCoinQuoteCoinbase("BTC", "EUR");
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        editor.putFloat("btcusd", btcusd);
        editor.putFloat("btceur", btceur);
        editor.apply();

        coin = new Coin(getApplicationContext(), true);

        progressBar.setProgress(progressBarProgess += progressBarSlotTime);

        for (int i = 0; i < coin.coins.length; i ++) {
            float amountBought = prefs.getFloat(coin.coins[i] + "_a", -1f);

            if (amountBought != -1f) {
                String altcoinCurrency = prefs.getString(coin.coins[i] + "_currency", "EUR");
                float currentUnitValue = coin.getCurrentCoinValue(coin.coins[i], altcoinCurrency);
                editor.putFloat(coin.coins[i] + "_currentBalance", currentUnitValue);
                editor.commit();

                progressBarProgess += progressBarSlotTime;
                progressBar.setProgress(progressBarProgess);

                /*Log.d("LOADING", "coin.coins[i]: " + coin.coins[i]);
                Log.d("LOADING", "amountBought: " + amountBought);
                Log.d("LOADING", "altcoinCurrency: " + altcoinCurrency);
                Log.d("LOADING", "currentUnitValue: " + currentUnitValue);*/
            }
        }
    }

}
